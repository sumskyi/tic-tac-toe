require 'singleton'
require 'tic_tac_toe/point'

module TicTacToe
  class Board
    DuplicateValueError = Class.new(StandardError)
    FinishedGameError   = Class.new(StandardError)

    SIZE = 3

    WINNER_POSITIONS = [
      # whole rows
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],

      # whole columns
      [[0, 0], [1, 0], [2, 0]],
      [[1, 0], [1, 1], [2, 1]],
      [[1, 0], [1, 1], [2, 1]],

      # diag
      [[0, 0], [1, 1], [2, 2]],
      [[0, 2], [1, 1], [2, 0]]
    ]

    attr_reader :board, :winner_sign

    def initialize
      @board = [Array.new(SIZE),
                Array.new(SIZE),
                Array.new(SIZE)]
    end

    def set(point, value)
      raise DuplicateValueError unless self[point].nil?
      raise FinishedGameError if finished?

      @board[point.x][point.y] = value
    end

    def [](point)
      @board[point.x][point.y]
    end

    def finished?
      won? or @board.flatten.all?{|cell| not cell.nil?}
    end

    def won?
      WINNER_POSITIONS.each do |positions|
        point1 = Point.new(*positions[0])
        point2 = Point.new(*positions[1])
        point3 = Point.new(*positions[2])

        current_sign = self[point1]

        if equals(point1, point2, point3)
          @winner_sign = current_sign
          return true
        end
      end

      false
    end

    def equals(point1, point2, point3)
      return false if self[point1].nil?

      self[point1] == self[point2] && self[point2] == self[point3]
    end

    def to_a
      [@board[0], @board[1], @board[2]]
    end

  end
end
