require 'tic_tac_toe/player'
require 'tic_tac_toe/board'

module TicTacToe
  class Game
    attr_reader :players, :board

    def initialize
      @players = {
        :X => Player.new(:X),
        :O => Player.new(:O)
      }

      @board = Board.new
    end

    def turn(point, player)
      @board.set(point, player.sign)
    end

    def finished?
      @board.finished?
    end

    def winner
      finished? && @players[@board.winner_sign]
    end

  end
end
