require 'spec_helper'
require 'tic_tac_toe/point'

describe TicTacToe::Point do
  subject(:point) { TicTacToe::Point.new(0, 0) }

  its(:x) { should eq 0 }
  its(:y) { should eq 0 }
end
