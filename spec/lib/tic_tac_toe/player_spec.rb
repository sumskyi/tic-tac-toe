require 'spec_helper'
require 'tic_tac_toe/player'

describe TicTacToe::Player do
  let(:sign) { :X }
  subject(:player) {  TicTacToe::Player.new(sign) }

  its(:sign) { should == sign }
end
