require 'spec_helper'
require 'tic_tac_toe/board'

describe TicTacToe::Board do
  subject(:board) { TicTacToe::Board.new }
  let(:point) { TicTacToe::Point.new(0, 0) }

  it 'read value' do
    board[point].should == nil
  end

  it 'set value' do
    board.set(point, 'X')
    board[point].should == 'X'
  end

  it 'can not set value twice' do
    board.set(point, 'X')
    expect {
      board.set(point, 'X')
    }.to raise_error(TicTacToe::Board::DuplicateValueError)
  end

  it 'can not set value on finished game' do
    board.stub :finished? => true
    expect {
      board.set(point, 'X')
    }.to raise_error(TicTacToe::Board::FinishedGameError)
  end

  describe 'winner positions' do
    let(:point2) { TicTacToe::Point.new(0, 1) }
    let(:point3) { TicTacToe::Point.new(0, 2) }

    it 'is win' do
      board.set(point,  :X)
      board.set(point2, :X)
      board.set(point3, :X)
      board.won?.should be_true
    end

    it 'is no winner' do
      board.won?.should be_false
    end
  end
end

