require 'spec_helper'
require 'tic_tac_toe/game'

describe TicTacToe::Game do
  subject(:game) { TicTacToe::Game.new }
  let(:player) { game.players[:X] }
  let(:point) { TicTacToe::Point.new(0, 0) }
  let(:point2) { TicTacToe::Point.new(0, 1) }
  let(:point3) { TicTacToe::Point.new(0, 2) }

  it '#turn' do
    game.board.should_receive(:set)
    game.turn(point, player)
  end

  describe '#winner' do
    it 'get it' do
      game.turn(point,  player)
      game.turn(point2, player)
      game.turn(point3, player)

      game.winner.should == player
    end

    it 'is no winner' do
      game.winner.should_not be
    end
  end
end
