Feature: Tic Tac Toe game
  Let's play new Tic-Tac-Toe game

  Scenario: player X wins
    Given new game started
    And "playerX" fills the board with:
      | | | |
      | |X| |
      | | | |
    And "playerO" fills the board with:
      | | |0|
      | | | |
      | | | |
    And "playerX" fills the board with:
      | | | |
      |X| | |
      | | | |
    And "playerO" fills the board with:
      | |O| |
      | | | |
      | | | |
    And "playerX" fills the board with:
      | | | |
      | | |X|
      | | | |
    And debug the board
    And the game should ends
    And the winner is "playerX"

  Scenario: no winner
    Given new game started
    And "playerX" fills the board with:
      |X| | |
      | | | |
      | | | |
    And "playerO" fills the board with:
      | | |0|
      | | | |
      | | | |
    And "playerX" fills the board with:
      | |X| |
      | | | |
      | | | |
    And "playerO" fills the board with:
      | | | |
      |O| | |
      | | | |
    And "playerX" fills the board with:
      | | | |
      | |X| |
      | | | |
    And "playerO" fills the board with:
      | | | |
      | | | |
      | |O| |
    And "playerX" fills the board with:
      | | | |
      | | |X|
      | | | |
    And "playerO" fills the board with:
      | | | |
      | | | |
      | | |O|
    And "playerX" fills the board with:
      | | | |
      | | | |
      |X| | |
    And debug the board
    And the game should ends
    And there is no winner

