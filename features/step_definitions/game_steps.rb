Given(/^new game started$/) do
  @game = TicTacToe::Game.new
  # reset board, testing singleton
  @playerX = @game.players[:X]
  @player0 = @game.players[:O]
end

Given(/^"(.*?)" fills the board with:$/) do |player, table|
  player = @game.players[player[-1].intern]
  table.raw.each_with_index do |row, row_idx|
    row.each_with_index do |cell, col_idx|
      unless cell.empty?
        point = TicTacToe::Point.new(row_idx, col_idx)
        @game.turn(point, player)
      end
    end
  end
end

Given(/^debug the board$/) do
  puts Hirb::Helpers::AutoTable.render @game.board.to_a
end

Given(/^the game should ends$/) do
  @game.finished?
end

Given(/^the winner is "(.*?)"$/) do |player|
  player = @game.players[player[-1].intern]
  @game.winner.should == player
end

Given(/^there is no winner$/) do
  @game.winner.should be_nil
end

